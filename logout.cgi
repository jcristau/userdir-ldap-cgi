#!/usr/bin/perl

# $Id: logout.cgi,v 1.2 1999/09/26 01:20:39 tausq Exp $
# (c) 1999 Randolph Chung. Licensed under the GPL. <tausq@debian.org>
# Copyright (c) 2011, 2015 Peter Palfrader

use lib '.';
use strict vars;
#use Apache::Registry;
use CGI;
use Util;
use Net::LDAP qw(LDAP_SUCCESS LDAP_PROTOCOL_ERROR);

my %config = &Util::ReadConfigFile;
my $proto = ($ENV{HTTPS} ? "https" : "http");

my $query = new CGI;
my $authtoken = $query->param('authtoken');
&Util::ClearAuthToken($authtoken);

my $doneurl = $config{websearchurl};
print "Location: $proto://$ENV{SERVER_NAME}/$doneurl\n\n";
